package com.itheima.shoppingapp.bean;

import java.io.Serializable;

/**
 * Created by CC on 2016/9/21.
 * 首页热门活动
 */
public class Campaign extends BaseBean{

    /**
     * title : 超值购
     * cpOne : {"id":17,"title":"手机专享","imgUrl":"http://7mno4h.com2.z0.glb.qiniucdn.com/555c6c90Ncb4fe515.jpg"}
     * cpTwo : {"id":15,"title":"闪购","imgUrl":"http://7mno4h.com2.z0.glb.qiniucdn.com/560a26d2N78974496.jpg"}
     * cpThree : {"id":11,"title":"团购","imgUrl":"http://7mno4h.com2.z0.glb.qiniucdn.com/560be0c3N9e77a22a.jpg"}
     */

    private String title;
    /**
     * id : 17
     * title : 手机专享
     * imgUrl : http://7mno4h.com2.z0.glb.qiniucdn.com/555c6c90Ncb4fe515.jpg
     */

    private ItemBean cpOne;
    /**
     * id : 15
     * title : 闪购
     * imgUrl : http://7mno4h.com2.z0.glb.qiniucdn.com/560a26d2N78974496.jpg
     */

    private ItemBean cpTwo;
    /**
     * id : 11
     * title : 团购
     * imgUrl : http://7mno4h.com2.z0.glb.qiniucdn.com/560be0c3N9e77a22a.jpg
     */

    private ItemBean cpThree;

    public ItemBean getCpOne() {
        return cpOne;
    }

    public ItemBean getCpTwo() {
        return cpTwo;
    }

    public ItemBean getCpThree() {
        return cpThree;
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "title='" + title + '\'' +
                ", cpOne=" + cpOne +
                ", cpTwo=" + cpTwo +
                ", cpThree=" + cpThree +
                '}';
    }

    public static class ItemBean implements Serializable {
        private int id;
        private String title;
        private String imgUrl;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        @Override
        public String toString() {
            return "ItemBean{" +
                    "id=" + id +
                    ", title='" + title + '\'' +
                    ", imgUrl='" + imgUrl + '\'' +
                    '}';
        }
    }
}
