package com.itheima.shoppingapp.bean;

/**
 * Created by CC on 2016/9/9.
 * Bottom bar tabhost
 */
public class TabhostBean {
    /**
     * 显示的title
     */
    public int tilte;
    /**
     * 显示的图标
     */
    public int icon;
    /**
     * 需要显示的Fragment
     */
    public Class fragment;

    public TabhostBean(int tilte, int icon, Class fragment) {
        this.tilte = tilte;
        this.icon = icon;
        this.fragment = fragment;
    }
}
