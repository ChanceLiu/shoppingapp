package com.itheima.shoppingapp.bean;

/**
 * Created by CC on 2016/9/25.
 */
public class ShoppingCart {

    public   int id;
    public String name;
    public String imgUrl;
    public String description;
    public String price;
    public String sale;
    public int number;
    public boolean isSelected;

    public ShoppingCart(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.imgUrl = product.getImgUrl();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.sale = product.getSale();
        this.number = 1;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShoppingCart that = (ShoppingCart) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
