package com.itheima.shoppingapp.http;

/**
 * Created by CC on 2016/9/21.
 */
public class UrlUtils {

    /**
     * 基础公共地址
     */
    public static final String BASE_URL = "http://112.124.22.238:8081/course_api/";
    /**
     * 轮播图
     */
    public static final String BANNER = "banner/query";
    /**
     * 首页热门活动
     */
    public static final String CAMPAIGN = "campaign/recommend";
    /**
     * 热卖商品
     */
    public static final String HOT_PRODUCT = "wares/hot";
    /**
     * 一级商品分类
     * http://112.124.22.238:8081/course_api/category/list
     */
    public static final String CATEGORY_ONE = "category/list";

     /**
     * 一级商品分类
     * http://112.124.22.238:8081/course_api/wares/list
     */
    public static final String CATEGORY_PRODUCT = "wares/list";

     /**
     * 首页热门活动中的
     * http://112.124.22.238:8081/course_api/wares/campaign/list商品
     */
    public static final String CAMPAIGN_CATEGORY_PRODUCT = "wares/campaign/list";


}
