package com.itheima.shoppingapp.http;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by CC on 2016/9/21.
 * <p/>
 * 提供HttpApi实例的
 */
public class HttpServiceCreator {


    private static HttpApi httpApi;

    /**
     * 获取api
     *
     * @return
     */
    public static HttpApi getHttpApi() {
        if (httpApi == null)
            httpApi = new Retrofit.Builder()
                    .baseUrl(UrlUtils.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(HttpApi.class);
        return httpApi;

    }
}
