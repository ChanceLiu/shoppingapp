package com.itheima.shoppingapp.http;

import com.itheima.shoppingapp.bean.Banner;
import com.itheima.shoppingapp.bean.Campaign;
import com.itheima.shoppingapp.bean.CategoryBean;
import com.itheima.shoppingapp.bean.CategoryProducts;
import com.itheima.shoppingapp.bean.HotSale;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by CC on 2016/9/21.
 */
public interface HttpApi {

    /**
     * 轮播图
     *
     * @param type
     * @return
     */
    @GET(UrlUtils.BANNER)
    Call<List<Banner>> getHomeBanner(@Query("type") String type);

    /**
     * @return 首页热门活动
     */
    @GET(UrlUtils.CAMPAIGN)
    Call<List<Campaign>> getCampaign();


    /**
     * @return 热卖商品
     */
    @FormUrlEncoded
    @POST(UrlUtils.HOT_PRODUCT)
    Call<HotSale> getHotSale(@Field("curPage") int curPage, @Field("pageSize") int pageSize);


    /**
     * @return 一级商品分类
     */
    @GET(UrlUtils.CATEGORY_ONE)
    Call<List<CategoryBean>> getCategoryLevelOne();


    /**
     * @param categoryId 分类的ID
     * @param curPage    当前页
     * @param pageSize   页大小
     * @return 分类商品
     */
    @GET(UrlUtils.CATEGORY_PRODUCT)
    Call<CategoryProducts> getCategoryProduct(@Query("categoryId") int categoryId, @Query("curPage") int curPage, @Query("pageSize") int pageSize);

    /**
     * @return  首页推荐商品
     */
    @GET(UrlUtils.CAMPAIGN_CATEGORY_PRODUCT)
    Call<CategoryProducts> getCampaignProduct(@QueryMap Map<String, String> params);


}
