package com.itheima.shoppingapp.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.TintTypedArray;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.itheima.shoppingapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by CC on 2016/9/9.
 */
public class HeimaToolBar extends Toolbar {
    public OnRightButtonClickListener onRightButtonClickListener;
    @BindView(R.id.toolbar_searchview)
    EditText BKToolbarSearchview;
    @BindView(R.id.toolbar_title)
    TextView BKToolbarTitle;
    @BindView(R.id.toolbar_rightButton)
    ImageButton BKToolbarRightButton;
    @BindView(R.id.toolbar_rightTextButton)
    TextView BKToolbarTextBtn;
    private String titleStr;
    private boolean isShowSearchView;
    private String rightButtonText;

    public HeimaToolBar(Context context) {
        this(context, null);
    }

    public HeimaToolBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HeimaToolBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView();

        setContentInsetsRelative(16, 16);

        if (attrs != null) {
            TintTypedArray a = TintTypedArray.obtainStyledAttributes(getContext(), attrs,
                    R.styleable.HeimaToolBar, defStyleAttr, 0);

            titleStr = a.getString(R.styleable.HeimaToolBar_centerTitle);
            if (titleStr != null)
                setTitle(titleStr);

            isShowSearchView = a.getBoolean(R.styleable.HeimaToolBar_isShowSearchView, false);
            if (isShowSearchView) {
                showSearchView();
                hideTitleView();
            } else {
                showTitleView();
                hideSearchView();
            }

            rightButtonText = a.getString(R.styleable.HeimaToolBar_rightButtonText);
            if (!TextUtils.isEmpty(rightButtonText)) {
                setRightButton(rightButtonText);
            }
            Drawable drawable = a.getDrawable(R.styleable.HeimaToolBar_rightButton);
            if (drawable != null) {
                setRightButton(drawable);
            }

            a.recycle();
        }
    }

    public void setRightButton(String rightButtonText) {
        BKToolbarTextBtn.setVisibility(VISIBLE);
        BKToolbarTextBtn.setText(rightButtonText);

    }

    /**
     * 初始化View
     */
    private void initView() {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.toolbar, this);
//        LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL);
//
//        addView(mView, lp);
        //使用butterknife绑定
        ButterKnife.bind(this, mView);


    }

    @Override
    public void setTitle(CharSequence title) {
        BKToolbarTitle.setText(title);
    }

    @Override
    public void setTitle(@StringRes int resId) {
        titleStr = getContext().getString(resId);
        setTitle(titleStr);
    }

    public void showTitleView() {
        BKToolbarTitle.setVisibility(VISIBLE);
    }

    public void hideTitleView() {
        BKToolbarTitle.setVisibility(GONE);
    }

    public void showSearchView() {
        BKToolbarSearchview.setVisibility(VISIBLE);
    }

    public void hideSearchView() {
        BKToolbarSearchview.setVisibility(GONE);
    }

    public void setRightButton(Drawable drawable) {
        BKToolbarRightButton.setVisibility(VISIBLE);
        BKToolbarRightButton.setImageDrawable(drawable);
    }

    public void setRightButton(Drawable drawable,int tag) {
        BKToolbarRightButton.setVisibility(VISIBLE);
        BKToolbarRightButton.setImageDrawable(drawable);
        BKToolbarRightButton.setTag(tag);
    }


    @OnClick({R.id.toolbar_rightButton, R.id.toolbar_rightTextButton})
    public void onRightBtnClick(View view) {
        if (onRightButtonClickListener != null) {
            onRightButtonClickListener.onClick(view);
        }
    }

    public void setOnRightButtonClickListener(OnRightButtonClickListener onRightButtonClickListener) {
        this.onRightButtonClickListener = onRightButtonClickListener;
    }

    public interface OnRightButtonClickListener {
        void onClick(View view);
    }
}
