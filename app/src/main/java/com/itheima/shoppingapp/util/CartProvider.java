package com.itheima.shoppingapp.util;

import android.content.Context;
import android.util.SparseArray;

import com.google.gson.reflect.TypeToken;
import com.itheima.shoppingapp.bean.Product;
import com.itheima.shoppingapp.bean.ShoppingCart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by <a href="http://www.cniao5.com">菜鸟窝</a>
 * 一个专业的Android开发在线教育平台
 */
public class CartProvider {


    public static final String CART_JSON = "cart_json";

    private SparseArray<ShoppingCart> datas = null;


    private Context mContext;


    public CartProvider(Context context) {

        mContext = context;
        datas = new SparseArray<>(10);
        listToSparse();

    }


    public void put(ShoppingCart cart) {


        ShoppingCart temp = datas.get(cart.id);

        if (temp != null) {
            temp.number++;
        } else {
            temp = cart;
            temp.number = 1;
        }

        datas.put(cart.id, temp);

        commit();

    }


    public void put(Product wares) {


        ShoppingCart cart = convertData(wares);
        put(cart);
    }

    public void update(ShoppingCart cart) {

        datas.put(cart.id, cart);
        commit();
    }

    public void delete(ShoppingCart cart) {
        datas.delete(cart.id);
        commit();
    }

    public List<ShoppingCart> getAll() {

        return getDataFromLocal();
    }


    public void commit() {


        List<ShoppingCart> carts = sparseToList();

        PreferencesUtils.putString(mContext, CART_JSON, JSONUtil.toJSON(carts));

    }


    private List<ShoppingCart> sparseToList() {


        int size = datas.size();

        List<ShoppingCart> list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {

            list.add(datas.valueAt(i));
        }
        return list;

    }


    private void listToSparse() {

        List<ShoppingCart> carts = getDataFromLocal();

        if (carts != null && carts.size() > 0) {

            for (ShoppingCart cart :
                    carts) {

                datas.put(cart.id, cart);
            }
        }

    }


    public List<ShoppingCart> getDataFromLocal() {

        String json = PreferencesUtils.getString(mContext, CART_JSON);
        List<ShoppingCart> carts = new ArrayList<>();
        if (json != null) {

            carts = JSONUtil.fromJson(json, new TypeToken<List<ShoppingCart>>() {
            }.getType());

        }

        return carts;

    }


    public ShoppingCart convertData(Product item) {

        ShoppingCart cart = new ShoppingCart(item);


        return cart;
    }


}
