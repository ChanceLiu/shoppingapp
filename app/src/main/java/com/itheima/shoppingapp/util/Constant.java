package com.itheima.shoppingapp.util;

/**
 * Created by CC on 2016/9/25.
 */
public class Constant {

    public static final String SHOP_CART_SP = "shop_cart_sp";
    public static final String SHOP_CART = "shop_cart";
    public static final String CAMPAIGN_ITEM = "campaign_item";
    public static final String PRODUCT = "product";
}
