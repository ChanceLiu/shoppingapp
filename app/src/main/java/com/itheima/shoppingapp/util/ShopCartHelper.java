package com.itheima.shoppingapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.itheima.shoppingapp.bean.ShoppingCart;

/**
 * Created by CC on 2016/9/25.
 */
public class ShopCartHelper {
    private static SharedPreferences sharedPreferences;
    private static ShopCartHelper helper;

    private static SparseArray<ShoppingCart> shopCarts;

    private ShopCartHelper(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constant.SHOP_CART_SP, Context.MODE_PRIVATE);
        }
        shopCarts = new SparseArray<>(10);
    }

    public static ShopCartHelper getInstance(Context context) {
        if (helper == null)
            helper = new ShopCartHelper(context);
        return helper;
    }


    public void addCart(ShoppingCart products) {
        ShoppingCart shopProducts = shopCarts.get(products.id);
        if (shopProducts == null) {
            shopCarts.put(products.id, products);
        } else {
            ++shopProducts.number;
        }

        saveData();
    }

    private void saveData() {
        String json = new Gson().toJson(shopCarts);
        sharedPreferences.edit().putString(Constant.SHOP_CART, json).commit();
    }
}
