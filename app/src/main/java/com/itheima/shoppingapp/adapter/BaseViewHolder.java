package com.itheima.shoppingapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by CC on 2016/9/22.
 */
public class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private SparseArray<View> mViews;
    private BaseAdapter.OnItemClickListener onItemClickListener;

    public BaseViewHolder(View itemView, BaseAdapter.OnItemClickListener onItemClickListener) {
        super(itemView);
        mViews = new SparseArray<>();
        itemView.setOnClickListener(this);
        this.onItemClickListener = onItemClickListener;
    }



    public TextView getTextView(int id) {
        return (TextView) getView(id);
    }

    public ImageView getImageView(int id) {
        return (ImageView) getView(id);
    }

    public Button getButton(int id) {
        return (Button) getClickableView(id);
    }
    /**
     * 查找View
     *
     * @param id
     * @return
     */
    public View getView(int id) {
        View view = mViews.get(id);
        if (view == null) {
            view = itemView.findViewById(id);
            mViews.put(id, view);
        }

        return view;
    }


    /**
     * 条目中如果你需要某个控件可以进行点击，可以调用这个方法
     * @param id
     * @return
     */
    public View getClickableView(int id) {
        View view = getView(id);
        view.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(view,getLayoutPosition());
        }
    }



}
