package com.itheima.shoppingapp.adapter;

import android.content.Context;

import java.util.List;

/**
 * Created by CC on 2016/9/22.
 * 简单Adapter,默认使用BaseViewHolder
 */
public abstract class SimpleAdapter<D> extends BaseAdapter<D, BaseViewHolder> {
    public SimpleAdapter(List<D> mData, Context mContext, int layoutId) {
        super(mData, mContext, layoutId);
    }
}
