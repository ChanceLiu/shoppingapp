package com.itheima.shoppingapp.adapter;

import android.content.Context;
import android.text.Html;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.itheima.numbereditorlib.NumberEditorView;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.bean.ShoppingCart;
import com.itheima.shoppingapp.activity.fragment.CartFragment;
import com.itheima.shoppingapp.util.CartProvider;
import com.itheima.shoppingapp.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by CC on 2016/9/25.
 */
public class ShopcartAdapter extends SimpleAdapter<ShoppingCart> {
    CartProvider mCartProvider;
    Context mContext;

    TextView textView;

    public ShopcartAdapter(List<ShoppingCart> mData, Context mContext) {
        super(mData, mContext, R.layout.template_cart);
        this.mContext = mContext;
        mCartProvider = new CartProvider(mContext);
    }


    @Override
    protected void bindData(final BaseViewHolder holder, final ShoppingCart shoppingCart) {

        Glide.with(mContext)
                .load(shoppingCart.imgUrl)
                .into(holder.getImageView(R.id.icon_product));
        holder.getTextView(R.id.text_title).setText(shoppingCart.name);
        holder.getTextView(R.id.text_price).setText(shoppingCart.price);
        final NumberEditorView numberEditorView = (NumberEditorView) holder.getView(R.id.num_control);
        numberEditorView.setmNumber(shoppingCart.number);
        numberEditorView.setOnNumberEditorClickListener(new NumberEditorView.OnNumberEditorClickListener() {
            @Override
            public void onAddButtonClick(int num) {
                mData.get(holder.getLayoutPosition()).number++;
                showTotalPrice();
            }

            @Override
            public void onSubButtonClick(int num) {
                mData.get(holder.getLayoutPosition()).number--;
                showTotalPrice();

            }

            @Override
            public void onNumberViewClick() {

            }

            @Override
            public void onFailed(int errorCode, int number) {
                String msg;
                if (number == NumberEditorView.LESS_THAN_MIN) {
                    msg = mContext.getString(R.string.less_than_min) + number;
                } else {
                    msg = mContext.getString(R.string.more_than_max) + number;

                }
                ToastUtil.showCenterToast(mContext, msg, Toast.LENGTH_SHORT);

            }
        });
        final CheckBox box = (CheckBox) holder.getView(R.id.checkbox);
        box.setChecked(shoppingCart.isSelected);
        box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                shoppingCart.isSelected = b;
                mCartProvider.update(shoppingCart);
                EventBus.getDefault().post(new CartFragment.ShopCartEvent(CartFragment.ShopCartEvent.SELECT_ALL_STATUE_CHANGE));
                showTotalPrice();
            }
        });
    }


    /**
     * @param isSelectAll 是否所有
     */
    public void selectAll(boolean isSelectAll) {
        for (ShoppingCart sc :
                mData) {
            sc.isSelected = isSelectAll;
        }
        showTotalPrice();
        notifyDataSetChanged();

    }


    /**
     * @return 是否全选
     */
    public boolean isAllItemSelected() {
        for (ShoppingCart sc :
                mData) {
            if (!sc.isSelected) return false;
        }
        return true;
    }

    public void setPriceTotalTv(TextView textView) {
        this.textView = textView;
    }

    /**
     * 显示总价格
     */
    public void showTotalPrice() {
        float total = getTotalPrice();
        textView.setText(Html.fromHtml("合计 ￥<span style='color:#eb4f38'>" + total + "</span>"), TextView.BufferType.SPANNABLE);
    }

    /**
     * @return
     * 计算总的价格
     */
    private float getTotalPrice() {
        float total = 0;
        for (ShoppingCart sc :
                mData) {
            if (sc.isSelected)
                total += (sc.number * Float.valueOf(sc.price));
        }
        return total;
    }

}
