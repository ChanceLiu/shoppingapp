package com.itheima.shoppingapp.adapter;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.bean.Product;

import java.util.List;

/**
 * Created by CC on 2016/9/26.
 */
public class ProductListAdapter extends SimpleAdapter<Product> {
    public ProductListAdapter(List<Product> mData, Context mContext) {
        super(mData, mContext,R.layout.template_hot_product);

    }
    public ProductListAdapter(List<Product> mData, Context mContext,int layoutId) {
        super(mData, mContext,layoutId);

    }

    @Override
    protected void bindData(BaseViewHolder holder, Product product) {
        Glide.with(mContext)
                .load(product.getImgUrl())
                .into(holder.getImageView(R.id.icon_product));

        holder.getTextView(R.id.text_title).setText(product.getName());
        holder.getTextView(R.id.text_price).setText(product.getPrice());
    }

}
