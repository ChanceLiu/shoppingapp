package com.itheima.shoppingapp.adapter;

import android.content.Context;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by CC on 2016/9/22.
 *
 * @param <D>
 * @param <VH>
 */
public abstract class BaseAdapter<D, VH extends BaseViewHolder> extends RecyclerView.Adapter<BaseViewHolder> {
    protected List<D> mData;
    protected Context mContext;
    protected int layoutId;
    /**
     * 条目点击监听器
     */
    private OnItemClickListener onItemClickListener;

    public BaseAdapter(List<D> mData, Context mContext, int layoutId) {
        this.layoutId = layoutId;
        this.mData = mData;
        this.mContext = mContext;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(mContext).inflate(layoutId, parent,null);
        View view = LayoutInflater.from(mContext).inflate(layoutId, parent, false);
        return new BaseViewHolder(view,onItemClickListener);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        bindData((VH) holder, mData.get(position));
    }

    public void addItem(List<D> data) {
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void refreshData(List<D> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();

    }
    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }

    /**
     * 绑定数据
     *
     * @param holder
     * @param d
     */
    protected abstract void bindData(VH holder, D d);

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * 具体条目的点击以及条目中具体控件的点击。
     */
    public interface OnItemClickListener {
        /**
         * @param view   点击的view
         * @param position 条目索引
         */
        void onItemClick(View view, int position);
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }
}
