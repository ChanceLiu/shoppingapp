package com.itheima.shoppingapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.bean.Campaign;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CC on 2016/9/14.
 * 在写RecycleView的时候，需要注意Adapter必须要继承RecyclerView.Adapter
 * <p>
 * 同时其中需要ViewHolder，ViewHolder必须要继承RecyclerView.ViewHolder
 */
public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.HomeCategoryViewHolder> {

    private static int VIEW_TYPE_L = 0;
    private static int VIEW_TYPE_R = 1;

    private Context context;
    private LayoutInflater inflater;


    private List<Campaign> data;
    private OnCampaignItemClickListener onCampaignItemClickListener;

    public HomeCategoryAdapter(Context context, List<Campaign> data) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;

    }

    @Override
    public HomeCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_L) {
            return new HomeCategoryViewHolder(inflater.inflate(R.layout.template_home_cardview, null));
        } else {
            return new HomeCategoryViewHolder(inflater.inflate(R.layout.template_home_cardview2, null));
        }
    }

    @Override
    public void onBindViewHolder(HomeCategoryViewHolder holder, int position) {
        Campaign category = data.get(position);
        Glide.with(context)
                .load(category.getCpOne().getImgUrl())
                .into(holder.BKImgviewBig);
        Glide.with(context)
                .load(category.getCpTwo().getImgUrl())
                .into(holder.BKImgviewSmallTop);
        Glide.with(context)
                .load(category.getCpThree().getImgUrl())
                .into(holder.BKImgviewSmallBottom);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return VIEW_TYPE_L;
        } else {
            return VIEW_TYPE_R;
        }

    }

    public void addItem(List<Campaign> campaigns) {
        data.clear();
        data.addAll(campaigns);
        notifyDataSetChanged();

    }

    /**
     * 设置条目监听器
     *
     * @param onCampaignItemClickListener
     */
    public void setOnCampaignItemClickListener(OnCampaignItemClickListener onCampaignItemClickListener) {
        this.onCampaignItemClickListener = onCampaignItemClickListener;
    }

    public interface OnCampaignItemClickListener {
        void onCampaignClick(View view,int position, Campaign.ItemBean item);
    }

    public class HomeCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.text_title)
        TextView BKTextTitle;
        @BindView(R.id.imgview_big)
        ImageView BKImgviewBig;
        @BindView(R.id.imgview_small_top)
        ImageView BKImgviewSmallTop;
        @BindView(R.id.imgview_small_bottom)
        ImageView BKImgviewSmallBottom;

        public HomeCategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            BKImgviewBig.setOnClickListener(this);
            BKImgviewSmallBottom.setOnClickListener(this);
            BKImgviewSmallTop.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onCampaignItemClickListener != null) {
                int position = getLayoutPosition();
                Campaign campaign = data.get(position);
                switch (view.getId()) {
                    case R.id.imgview_big:
                        onCampaignItemClickListener.onCampaignClick(view,position,campaign.getCpOne());
                        break;
                    case R.id.imgview_small_bottom:
                        onCampaignItemClickListener.onCampaignClick(view,position,campaign.getCpThree());
                        break;
                    case R.id.imgview_small_top:
                        onCampaignItemClickListener.onCampaignClick(view,position,campaign.getCpTwo());
                        break;

                }
            }
        }
    }

}
