package com.itheima.shoppingapp.adapter;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.bean.Product;

import java.util.List;

/**
 * Created by CC on 2016/9/24.
 * 商品分类的适配器
 */
public class CategoryProductAdapter extends SimpleAdapter<Product> {
    public CategoryProductAdapter(List<Product> mData, Context mContext) {
        super(mData, mContext, R.layout.template_grid_product);
    }

    @Override
    protected void bindData(BaseViewHolder holder, Product product) {

        holder.getTextView(R.id.text_title).setText(product.getName());
        holder.getTextView(R.id.text_price).setText(product.getPrice());
        Glide.with(mContext)
                .load(product.getImgUrl())
                .placeholder(R.drawable.placeholder)
                .into(holder.getImageView(R.id.icon_product));

    }
}
