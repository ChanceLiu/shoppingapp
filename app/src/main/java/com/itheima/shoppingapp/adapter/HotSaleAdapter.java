package com.itheima.shoppingapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.bean.Product;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CC on 2016/9/21.
 */
public class HotSaleAdapter extends RecyclerView.Adapter<HotSaleAdapter.MViewHolder> {

    private List<Product> data;
    private Context context;

    public HotSaleAdapter(List<Product> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public MViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.template_hot_product, null);
        return new MViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MViewHolder holder, int position) {
        Product hotProduct = data.get(position);
        Glide.with(context)
                .load(hotProduct.getImgUrl())
                .into(holder.BKIvIcon);

        holder.BKTextTitle.setText(hotProduct.getName());
        holder.BKTextPrice.setText(hotProduct.getPrice());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.icon_product)
        ImageView BKIvIcon;
        @BindView(R.id.text_title)
        TextView BKTextTitle;
        @BindView(R.id.text_price)
        TextView BKTextPrice;
        @BindView(R.id.btn_add)
        Button BKBtnAdd;

        public MViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            BKBtnAdd.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) {
                int position = getLayoutPosition();
                switch (view.getId()) {
                    case R.id.btn_add:
                        itemClickListener.onItemBtnClickListener(position, data.get(position));
                        break;
                    default:
                        itemClickListener.onItemClick(position,data.get(position));
                }
            }
        }

    }

    public OnItemClickListener itemClickListener;

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    /**
     *条目点击事件
     */
    public interface OnItemClickListener{

        /**
         * 条目点击事件
         * @param position
         * @param product
         */
        void onItemClick(int position, Product product);

        /**
         * 条目Button点击事件
         * @param position
         * @param product
         */
        void onItemBtnClickListener(int position, Product product);
    }
}
