package com.itheima.shoppingapp.activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.itheima.shoppingapp.util.ToastUtil;

import butterknife.ButterKnife;


/**
 * Created by Ivan on 15/9/22.
 */
public abstract  class BaseFragment extends Fragment{


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view= inflater.inflate(getLayout(),container,false);
        ButterKnife.bind(this, view);
        return view ;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }
    abstract int getLayout();

    protected abstract void initData();

    /**
     * @param msg
     * 显示信息
     */
    protected void showMsg(String msg) {
        ToastUtil.showToast(getContext(),msg, Toast.LENGTH_SHORT);
    }

}
