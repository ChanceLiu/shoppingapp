package com.itheima.shoppingapp.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.adapter.BaseAdapter;
import com.itheima.shoppingapp.adapter.ProductListAdapter;
import com.itheima.shoppingapp.bean.Campaign;
import com.itheima.shoppingapp.bean.CategoryProducts;
import com.itheima.shoppingapp.bean.Product;
import com.itheima.shoppingapp.http.HttpServiceCreator;
import com.itheima.shoppingapp.util.CartProvider;
import com.itheima.shoppingapp.util.Constant;
import com.itheima.shoppingapp.util.ToastUtil;
import com.itheima.shoppingapp.widget.HeimaToolBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListActivity extends BaseActivity {

    public static final int ORDER_BY_DEFAULT = 0;
    public static final int ORDER_BY_SALE = 1;
    public static final int ORDER_BY_PRICE = 2;
    public static final int TYPE_LINE = 0;
    public static final int TYPE_GRID = 2;
    @BindView(R.id.toolbar)
    HeimaToolBar BKToolbar;
    @BindView(R.id.tab_layout)
    TabLayout BKTabLayout;
    @BindView(R.id.txt_summary)
    TextView BKTxtSummary;
    @BindView(R.id.recycler_view)
    RecyclerView BKRecyclerView;
    @BindView(R.id.refresh_layout)
    MaterialRefreshLayout BKRefreshLayout;
    @BindView(R.id.empty_view)
    View emptyView;
    LoadStyle loadStyle = LoadStyle.NORMAL;
    private CartProvider mCartProvider;
    private int mCurPage = 1;
    private int mPageSize = 15;
    private int mCampaignId;
    private ProductListAdapter mAdapter;
    private List<Product> mData;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context mContext;
    private int mOderBy;
    private int mTotalPage;
    private BaseAdapter.OnItemClickListener onItemClickListener = new BaseAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            Product product = mData.get(position);
            switch (view.getId()) {
                case R.id.btn_add:
                    mCartProvider.put(product);
                    break;
                default:
                    Intent intent = new Intent(ProductListActivity.this, ProductDetailActivity.class);
                    intent.putExtra(Constant.PRODUCT, product);
                    startActivity(intent);
                    break;
            }
        }
    };

    @Override
    int getLayoutResID() {
        return R.layout.activity_product_list;
    }

    @Override
    protected void initWidget() {
        mContext = this;

        BKToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        initTab();
        mData = new ArrayList<>();
        mAdapter = new ProductListAdapter(mData, this);
        mLayoutManager = new LinearLayoutManager(this);
        BKRecyclerView.setLayoutManager(mLayoutManager);
        BKRecyclerView.setAdapter(mAdapter);

        //BKRefreshLayout
        BKRefreshLayout.setLoadMore(true);
        BKRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                loadStyle = LoadStyle.REFRESH;

                requestData(mCampaignId, mOderBy, mCurPage = 1, mPageSize);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                super.onRefreshLoadMore(materialRefreshLayout);
                if (mCurPage < mTotalPage) {
                    loadStyle = LoadStyle.LOAD_MORE;
                    requestData(mCampaignId, mOderBy, ++mCurPage, mPageSize);
                } else {
                    ToastUtil.showCenterToast(mContext, "到底了呢！！", 1500);
                    finishLoad();
                }

            }
        });
        changeLayout(TYPE_LINE);
        BKToolbar.setOnRightButtonClickListener(new HeimaToolBar.OnRightButtonClickListener() {
            @Override
            public void onClick(View view) {
                int tag = (int) view.getTag();
                if (tag == TYPE_GRID) {
                    changeLayout(TYPE_LINE);
                } else {
                    changeLayout(TYPE_GRID);
                }
            }
        });


    }

    public void finishLoad() {
        BKRefreshLayout.finishRefresh();
        BKRefreshLayout.finishRefreshLoadMore();
    }

    /**
     * @param type 宫格和列表切换
     */
    private void changeLayout(int type) {
        if (type == TYPE_GRID) {
            mLayoutManager = new GridLayoutManager(this, 2);
            BKToolbar.setRightButton(getResources().getDrawable(R.mipmap.icon_grid_32), type);
            mAdapter = new ProductListAdapter(mData, this, R.layout.template_grid_product);
        } else {
            mLayoutManager = new LinearLayoutManager(this);
            BKToolbar.setRightButton(getResources().getDrawable(R.mipmap.icon_list_32), type);
            mAdapter = new ProductListAdapter(mData, this, R.layout.template_hot_product);

        }
        BKRecyclerView.setLayoutManager(mLayoutManager);
        BKRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(onItemClickListener);

    }

    @Override
    protected void initData() {
        mCartProvider = new CartProvider(this);

        Campaign.ItemBean item = (Campaign.ItemBean) getIntent().getExtras().getSerializable(Constant.CAMPAIGN_ITEM);
        mCampaignId = item.getId();
        loadStyle = LoadStyle.NORMAL;
        requestData(mCampaignId, mOderBy = ORDER_BY_DEFAULT, mCurPage, mPageSize);
    }

    private void requestData(int campaignId, int orderBy, int curPage, int pageSize) {
        Map<String, String> params = new HashMap<>();
        params.put("campaignId", "" + campaignId);
        params.put("orderBy", "" + orderBy);
        params.put("curPage", "" + curPage);
        params.put("pageSize", "" + pageSize);
        HttpServiceCreator.getHttpApi()
                .getCampaignProduct(params)
                .enqueue(new Callback<CategoryProducts>() {
                    @Override
                    public void onResponse(Call<CategoryProducts> call, Response<CategoryProducts> response) {
                        if (response.isSuccessful()) {
                            CategoryProducts products = response.body();
                            showProducts(products);
                        } else {
                            ToastUtil.showCenterToast(mContext, "请求发生错误", 1500);
                        }
                        finishLoad();
                    }

                    @Override
                    public void onFailure(Call<CategoryProducts> call, Throwable t) {
                        t.printStackTrace();
                        ToastUtil.showCenterToast(mContext, t.getMessage(), 1500);

                        finishLoad();
                    }
                });
    }

    private void initTab() {
        TabLayout.Tab tab = BKTabLayout.newTab();
        tab.setText("默认");
        tab.setTag(ORDER_BY_DEFAULT);
        BKTabLayout.addTab(tab);

        tab = BKTabLayout.newTab();
        tab.setText("价格");
        tab.setTag(ORDER_BY_PRICE);
        BKTabLayout.addTab(tab);

        tab = BKTabLayout.newTab();
        tab.setText("销量");
        tab.setTag(ORDER_BY_SALE);
        BKTabLayout.addTab(tab);

        BKTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                int orderBy = (int) tab.getTag();
                requestData(mCampaignId, orderBy, mCurPage = 1, mPageSize);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void showProducts(CategoryProducts products) {
        mTotalPage = products.getTotalPage();
        List<Product> list = products.getList();
        BKTxtSummary.setText("一共有" + products.getTotalCount() + "条数据");
        if (list.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
        switch (loadStyle) {
            case NORMAL:
                mAdapter.refreshData(list);
                break;
            case REFRESH:
                mAdapter.refreshData(list);
                break;
            case LOAD_MORE:
                mAdapter.addItem(list);
                break;
        }


    }

    enum LoadStyle {
        NORMAL, REFRESH, LOAD_MORE
    }

}
