package com.itheima.shoppingapp.activity.fragment;

import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.activity.ProductDetailActivity;
import com.itheima.shoppingapp.adapter.BaseAdapter;
import com.itheima.shoppingapp.adapter.BaseViewHolder;
import com.itheima.shoppingapp.adapter.SimpleAdapter;
import com.itheima.shoppingapp.bean.HotSale;
import com.itheima.shoppingapp.bean.Product;
import com.itheima.shoppingapp.http.HttpServiceCreator;
import com.itheima.shoppingapp.util.CartProvider;
import com.itheima.shoppingapp.util.Constant;
import com.itheima.shoppingapp.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Ivan on 15/9/22.
 */
public class HotFragment extends BaseFragment {


    private static final String TAG = "HotFragment";
    @BindView(R.id.recycle_view)
    RecyclerView BKRecycleView;
    @BindView(R.id.refresh_layout)
    MaterialRefreshLayout BKRefreshLayout;
    private int curPage = 1;
    private int pageSize = 10;

    /**
     * 数据加载状态
     */
    private Statues statue = Statues.NORMAL;
    private BaseAdapter mAdapter;
    /**
     * 适配器对应的数据
     */
    private List<Product> data;

    /**
     * RecylerView 的布局管理器
     */
    private RecyclerView.LayoutManager layoutManager;
    /**
     * 一共页数
     */
    private int totalPage;
    /**
     * RefreshLayout
     */
    private MaterialRefreshListener refreshListener = new MaterialRefreshListener() {
        @Override
        public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
            statue = Statues.REFRESH;
            //下拉刷新
            curPage = 1;
            requestHotProducts(curPage, pageSize);
        }


        @Override
        public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
            super.onRefreshLoadMore(materialRefreshLayout);
            statue = Statues.LOAD_MORE;
            //上啦加载
            if (curPage < totalPage) {
                requestHotProducts(++curPage, pageSize);
            } else {
                ToastUtil.showCenterToast(getActivity(), "没有更多数据了", Toast.LENGTH_SHORT);
                BKRefreshLayout.finishRefreshLoadMore();
            }


        }
    };
    private Toast toast;

    @Override
    int getLayout() {
        return R.layout.fragment_hot;
    }

    @Override
    protected void initData() {


        initRecyclerView();
        initMaterialRefreshLayout();
        requestHotProducts(curPage, pageSize);

    }

    private void initMaterialRefreshLayout() {
        BKRefreshLayout.setMaterialRefreshListener(refreshListener);
        BKRefreshLayout.setLoadMore(true);
    }

    /**
     * 初始化RecyclerView
     */
    private void initRecyclerView() {
        data = new ArrayList<>();
//        mAdapter = new HotSaleAdapter(data, getActivity());
        mAdapter=new SimpleAdapter<Product>(data,getActivity(),R.layout.template_hot_product){
            @Override
            protected void bindData(BaseViewHolder holder, Product product) {
                ImageView img = (ImageView) holder.getView(R.id.icon_product);
                TextView title = (TextView) holder.getView(R.id.text_title);
                TextView price = (TextView) holder.getView(R.id.text_price);
                Button button = holder.getButton(R.id.btn_add);
                Glide.with(getActivity()).load(product.getImgUrl()).into(img);
                title.setText(product.getName());
                price.setText(product.getPrice());
            }
        };
        //设置点击监听器
        mAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                        Log.d(TAG, "onItemClick: "+"view:" + view.toString() + "||position:" + position);
                Product product = data.get(position);
                switch (view.getId()) {
                    case R.id.btn_add:
                        new CartProvider(getContext()).put(product);
                        EventBus.getDefault().post(new CartFragment.ShopCartEvent(CartFragment.ShopCartEvent.UPDATE_DATA));
                        break;
                    default:
                        Intent intent=new Intent(getActivity(), ProductDetailActivity.class);
                        intent.putExtra(Constant.PRODUCT, product);
                        startActivity(intent);
                        break;
                }
            }
        });

        BKRecycleView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(getActivity());
        BKRecycleView.setLayoutManager(layoutManager);
        BKRecycleView.setItemAnimator(new DefaultItemAnimator());

    }

    /**
     * 请求网络数据
     *
     * @param curPage
     * @param pageSize
     */
    private void requestHotProducts(int curPage, int pageSize) {
        HttpServiceCreator
                .getHttpApi()
                .getHotSale(curPage, pageSize)
                .enqueue(new Callback<HotSale>() {
                    @Override
                    public void onResponse(Call<HotSale> call, Response<HotSale> response) {
                        if (response.isSuccessful()) {
                            HotSale hotSale = response.body();
                            showData(hotSale);
                            BKRefreshLayout.finishRefresh();
                            BKRefreshLayout.finishRefreshLoadMore();

                        }
                    }

                    @Override
                    public void onFailure(Call<HotSale> call, Throwable t) {
                        t.printStackTrace();
                        BKRefreshLayout.finishRefresh();
                        BKRefreshLayout.finishRefreshLoadMore();


                    }
                });
    }


    /**
     * 刷新显示数据
     *
     * @param hotSale
     */
    private void showData(HotSale hotSale) {
        switch (statue) {
            case NORMAL:
                totalPage = hotSale.getTotalPage();
                List<Product> latestData = hotSale.getList();
                mAdapter.addItem(latestData);
                break;
            case REFRESH:
                mAdapter.clear();
                mAdapter.addItem(hotSale.getList());
                break;
            case LOAD_MORE:
                mAdapter.addItem(hotSale.getList());
                BKRefreshLayout.finishRefreshLoadMore();
                break;
        }
    }



}
