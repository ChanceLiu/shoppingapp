package com.itheima.shoppingapp.activity.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.adapter.BaseAdapter;
import com.itheima.shoppingapp.adapter.BaseViewHolder;
import com.itheima.shoppingapp.adapter.CategoryProductAdapter;
import com.itheima.shoppingapp.adapter.SimpleAdapter;
import com.itheima.shoppingapp.bean.Banner;
import com.itheima.shoppingapp.bean.CategoryBean;
import com.itheima.shoppingapp.bean.CategoryProducts;
import com.itheima.shoppingapp.bean.Product;
import com.itheima.shoppingapp.http.HttpServiceCreator;
import com.itheima.shoppingapp.util.ToastUtil;
import com.itheima.shoppingapp.widget.HeimaToolBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Ivan on 15/9/22.
 */
public class CategoryFragment extends BaseFragment {


    @BindView(R.id.toolbar)
    HeimaToolBar BKToolbar;
    @BindView(R.id.recycler_view_category)
    RecyclerView BKRecyclerViewCategory;
    @BindView(R.id.recycler_view_product)
    RecyclerView BKRecyclerViewProduct;
    @BindView(R.id.refresh_layout)
    MaterialRefreshLayout BKRefreshLayout;
    @BindView(R.id.slider)
    SliderLayout BKSlider;

    private List<CategoryBean> mCategoryLO = new ArrayList<>();
    private BaseAdapter mCategoryAdapter;
    private int mCurrCategory=1;
    private int mCurPage = 1;
    private int mPageSize = 10;
    private int mTotalPage=0;


    /**
     * 多数据加载状态
     */
    private Statues mStatues = Statues.NORMAL;
    private BaseAdapter mProductAdapter;
    private List<Product> mProducts=new ArrayList<>();

    @Override
    int getLayout() {
        return R.layout.fragment_category;
    }

    @Override
    protected void initData() {
        initWidget();
        requestCategoryOne();
        requestBannerData();
        requestProduct(mCurrCategory, mCurPage, mPageSize);
    }


    /**
     * 初始化要用的Adapter，控件的初始化
     */
    private void initWidget() {
//----------------------分类的Adapter--------------------------
        mCategoryAdapter = new SimpleAdapter<CategoryBean>(mCategoryLO, getActivity(), R.layout.template_single_text) {
            @Override
            protected void bindData(BaseViewHolder holder, CategoryBean categoryBean) {
                holder.getTextView(R.id.textView).setText(categoryBean.getName());

            }

        };
//
        //设置分类的显示方式
        BKRecyclerViewCategory.setLayoutManager(new LinearLayoutManager(getContext()));
        //设置分类的点击事件
        mCategoryAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // TODO: 2016/9/22 分类点击
                CategoryBean categoryBean = mCategoryLO.get(position);
                System.out.println("分类：" + categoryBean);
                mCurrCategory = categoryBean.getId();
                mCurPage = 1;
                requestProduct(mCurrCategory,mCurPage,mPageSize);
            }
        });
        BKRecyclerViewCategory.setAdapter(mCategoryAdapter);
//------------------------商品的Adapter------------------------

        mProductAdapter = new CategoryProductAdapter(mProducts,getActivity());
        BKRecyclerViewProduct.setAdapter(mProductAdapter);
        BKRecyclerViewProduct.setLayoutManager(new GridLayoutManager(getContext(),2));


//---------------------加载刷新------------------
        //打开加载更多功能
        BKRefreshLayout.setLoadMore(true);
        BKRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                mStatues = Statues.REFRESH;
                mCurPage = 1;
                requestProduct(mCurrCategory,mCurPage,mPageSize);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                super.onRefreshLoadMore(materialRefreshLayout);
                mStatues = Statues.LOAD_MORE;
                if (mCurPage < mTotalPage) {
                    requestProduct(mCurrCategory, ++mCurPage, mPageSize);
                } else {
                    ToastUtil.showCenterToast(getContext(),getString(R.string.no_more_data),Toast.LENGTH_SHORT);
                    BKRefreshLayout.finishRefreshLoadMore();
                }
            }
        });

    }

    /**
     * 请求一级分类
     */
    private void requestCategoryOne() {
        HttpServiceCreator
                .getHttpApi()
                .getCategoryLevelOne()
                .enqueue(new Callback<List<CategoryBean>>() {
                    @Override
                    public void onResponse(Call<List<CategoryBean>> call, Response<List<CategoryBean>> response) {
                        if (response.isSuccessful()) {
                            List<CategoryBean> clOne = response.body();
                            showCategoryLevelOne(clOne);
                        } else {
                            showMsg("连接失败，为获取到分类信息");
                        }

                    }

                    @Override
                    public void onFailure(Call<List<CategoryBean>> call, Throwable t) {
                        showMsg("连接失败，为获取到分类信息");
                    }
                });
    }

    /**
     * @param clOne 显示一级分类
     */
    private void showCategoryLevelOne(List<CategoryBean> clOne) {
        mCategoryAdapter.refreshData(clOne);
    }

    /**
     * 获取轮播图的数据
     */
    private void requestBannerData() {
        String type = "1";
        HttpServiceCreator
                .getHttpApi()
                .getHomeBanner(type)
                .enqueue(new Callback<List<Banner>>() {
                    @Override
                    public void onResponse(Call<List<Banner>> call, Response<List<Banner>> response) {
                        if (response.isSuccessful()) {
                            List<Banner> bannerList = response.body();
                            showBannerData(bannerList);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Banner>> call, Throwable t) {

                    }
                });
    }

    /**
     * 初始化Banner数据
     *
     * @param bannerList
     */
    private void showBannerData(List<Banner> bannerList) {
        Banner banner = null;
        for (int i = 0; i < bannerList.size(); i++) {
            banner = bannerList.get(i);
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            textSliderView
                    .description(banner.getDescription())
                    .image(banner.getImgUrl()).setScaleType(BaseSliderView.ScaleType.Fit);
            textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {
                    Toast.makeText(getActivity(), slider.getDescription(), Toast.LENGTH_SHORT).show();
                }
            });
            BKSlider.addSlider(textSliderView);
        }
//        BKSlider.setCustomIndicator(BKCustomIndicator);//定义指示器
        BKSlider.setCustomAnimation(new DescriptionAnimation());//定义下面的说明view的动画
//        BKSlider.setPagerTransformer(false, new FlipHorizontalTransformer());////定义页面转场动画
        BKSlider.setDuration(2000);//设置停留时间
//        BKSlider.addOnPageChangeListener(pageChangeListener);//添加页面切换监听器
        BKSlider.startAutoCycle();//开始循环
    }




    /**
     * 获取指定分类商品列表
     *
     * @param categoryId
     * @param curPage
     * @param pageSize
     */
    private void requestProduct(int categoryId, int curPage, int pageSize) {
        HttpServiceCreator.getHttpApi()
                .getCategoryProduct(categoryId, curPage, pageSize)
                .enqueue(new Callback<CategoryProducts>() {
                    @Override
                    public void onResponse(Call<CategoryProducts> call, Response<CategoryProducts> response) {
                        if (response.isSuccessful()) {
                            CategoryProducts cateProducts = response.body();
                            showProduct(cateProducts);
                        } else {
                            showMsg("请求分类商品失败");
                        }
                        finishLoad();
                    }

                    @Override
                    public void onFailure(Call<CategoryProducts> call, Throwable t) {
                        showMsg("请求分类商品失败:"+t.getMessage());
                        finishLoad();
                    }
                });
    }

    public void finishLoad() {
        BKRefreshLayout.finishRefreshLoadMore();
        BKRefreshLayout.finishRefresh();
    }

    /**
     * @param cateProducts
     * 显示分类数据
     */
    private void showProduct(CategoryProducts cateProducts) {
        mTotalPage = cateProducts.getTotalPage();
        List<Product> productList = cateProducts.getList();
        switch (mStatues) {
            case NORMAL:
                mProductAdapter.clear();
                mProductAdapter.addItem(productList);
                break;
            case REFRESH:
                mProductAdapter.refreshData(productList);
                break;
            case LOAD_MORE:
                mProductAdapter.addItem(productList);
                break;

        }
    }

}



