package com.itheima.shoppingapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.activity.fragment.CartFragment;
import com.itheima.shoppingapp.activity.fragment.CategoryFragment;
import com.itheima.shoppingapp.activity.fragment.HomeFragment;
import com.itheima.shoppingapp.activity.fragment.HotFragment;
import com.itheima.shoppingapp.activity.fragment.MineFragment;
import com.itheima.shoppingapp.bean.TabhostBean;
import com.itheima.shoppingapp.widget.FragmentTabHost;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 在初始化Tabhost添加TabSpac的时候需要指定显示的Fragment，否则会造成
 *
 * @IllegalArgumentException you must specify a way to create the tab content
 */
public class MainActivity extends AppCompatActivity implements TabHost.OnTabChangeListener {

    @BindView(R.id.tabcontent)
    FrameLayout BKTabcontent;
    @BindView(android.R.id.tabhost)
    FrameLayout BKTabhost;
    @BindView(R.id.ftb_bottom_bar)
    FragmentTabHost BKFtbBottomBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //此步骤一定要
        BKFtbBottomBar.setup(this, getSupportFragmentManager(), R.id.tabcontent);
        initBottomBar();
        BKFtbBottomBar.setOnTabChangedListener(this);
    }

    /**
     * 初始化底部栏
     */
    private void initBottomBar() {

        List<TabhostBean> barHost = new ArrayList<>();
        barHost.add(new TabhostBean(R.string.home, R.drawable.selector_icon_home, HomeFragment.class));
        barHost.add(new TabhostBean(R.string.hot, R.drawable.selector_icon_hot, HotFragment.class));
        barHost.add(new TabhostBean(R.string.catagory, R.drawable.selector_icon_category, CategoryFragment.class));
        barHost.add(new TabhostBean(R.string.cart, R.drawable.selector_icon_cart, CartFragment.class));
        barHost.add(new TabhostBean(R.string.mine, R.drawable.selector_icon_mine, MineFragment.class));

        for (TabhostBean tab :
                barHost) {
            //创建TabSpec
            TabHost.TabSpec homeTab = BKFtbBottomBar.newTabSpec(getString(tab.tilte));
            //初始化TabSpec
            View tabView = View.inflate(this, R.layout.tab_indicator, null);
            ImageView tabIcon = (ImageView) tabView.findViewById(R.id.icon_tab);
            TextView tabTitle = (TextView) tabView.findViewById(R.id.txt_indicator);
            tabIcon.setImageResource(tab.icon);
            tabTitle.setText(tab.tilte);
            homeTab.setIndicator(tabView);//指定tab的显示View
            //需要加上指定显示那个页面，否则会报错：参数异常错误
            BKFtbBottomBar.addTab(homeTab, tab.fragment, null);

        }

    }


    @Override
    public void onTabChanged(String s) {
        EventBus.getDefault().post(new CartFragment.ShopCartEvent(CartFragment.ShopCartEvent.OFF_PAGE));
    }
}
