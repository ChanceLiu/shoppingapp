package com.itheima.shoppingapp.activity;

import android.app.Activity;
import android.os.Bundle;

import butterknife.ButterKnife;

/**
 * Created by CC on 2016/9/26.
 */
abstract class BaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResID());
        ButterKnife.bind(this);
        initWidget();
        initData();
    }

    abstract int getLayoutResID();


    /**
     * 初始化控件
     */
    protected abstract void initWidget();

    /**
     * 初始化数据，比如获取网络数据
     */
    protected abstract void initData();

}
