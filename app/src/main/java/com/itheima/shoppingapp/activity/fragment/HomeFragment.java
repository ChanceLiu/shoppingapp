package com.itheima.shoppingapp.activity.fragment;

import android.content.Intent;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.FlipHorizontalTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.activity.ProductListActivity;
import com.itheima.shoppingapp.adapter.HomeCategoryAdapter;
import com.itheima.shoppingapp.bean.Banner;
import com.itheima.shoppingapp.bean.Campaign;
import com.itheima.shoppingapp.http.HttpServiceCreator;
import com.itheima.shoppingapp.util.Constant;
import com.itheima.shoppingapp.widget.RecyclerViewHeader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ivan on 15/9/25.
 * <p>
 * AndroidImageSlider：轮播图的用法
 * https://github.com/daimajia/AndroidImageSlider/wiki/Getting-Start
 * <p>
 * RecyclerView + CardView 的用法。
 */
public class HomeFragment extends BaseFragment {

    /**
     * 页面改变监听器
     */
    public ViewPagerEx.OnPageChangeListener pageChangeListener = new ViewPagerEx.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
//            Toast.makeText(getActivity(), "page select:"+position, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    /**
     * recyclerView 监听器
     */
    public HomeCategoryAdapter.OnCampaignItemClickListener onCampaignItemClickListener = new HomeCategoryAdapter.OnCampaignItemClickListener() {
        @Override
        public void onCampaignClick(View view, int position, Campaign.ItemBean item) {
            Toast.makeText(getActivity(), item.getTitle(), Toast.LENGTH_SHORT).show();
            final Intent intent = new Intent(getActivity(), ProductListActivity.class);
            intent.putExtra(Constant.CAMPAIGN_ITEM, item);
            ViewCompat.animate(view)
                    .rotationXBy(360)
                    .setDuration(200)

                    .setListener(new ViewPropertyAnimatorListener() {
                        @Override
                        public void onAnimationStart(View view) {

                        }

                        @Override
                        public void onAnimationEnd(View view) {
                            startActivity(intent);
                        }

                        @Override
                        public void onAnimationCancel(View view) {

                        }
                    })
                    .start();

        }
    };
    @BindView(R.id.recycle_view)
    RecyclerView BKRecycleView;
    @BindView(R.id.recycle_view_header)
    RecyclerViewHeader BKRecycleViewHeader;
    @BindView(R.id.slider)
    SliderLayout BKSlider;
    @BindView(R.id.custom_indicator)
    PagerIndicator BKCustomIndicator;
    private RecyclerView.LayoutManager layoutManager;
    private HomeCategoryAdapter adapter;

    @Override
    protected void initData() {
        initRecycleView();
        initBanner();
        getBannerData();
        getCampaign();
    }

    @Override
    int getLayout() {
        return R.layout.fragment_home;
    }

    private void initBanner() {
        //把轮播图当作头添加到RecyclerView，一定要在设置了LayoutManager之后
        BKRecycleViewHeader.attachTo(BKRecycleView);
    }

    /**
     * 获取轮播图的数据
     */
    private void getBannerData() {
        String type = "1";
        HttpServiceCreator
                .getHttpApi()
                .getHomeBanner(type)
                .enqueue(new Callback<List<Banner>>() {
                    @Override
                    public void onResponse(Call<List<Banner>> call, Response<List<Banner>> response) {
                        if (response.isSuccessful()) {
                            List<Banner> bannerList = response.body();
                            initBannerData(bannerList);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Banner>> call, Throwable t) {

                    }
                });
    }

    /**
     * 获取热门数据
     */
    private void getCampaign() {
        HttpServiceCreator.getHttpApi()
                .getCampaign()
                .enqueue(new Callback<List<Campaign>>() {
                    @Override
                    public void onResponse(Call<List<Campaign>> call, Response<List<Campaign>> response) {
                        if (response.isSuccessful()) {
                            List<Campaign> campaignList = response.body();
                            adapter.addItem(campaignList);

                        }
                    }

                    @Override
                    public void onFailure(Call<List<Campaign>> call, Throwable t) {

                    }
                });
    }

    /**
     * 初始化Banner数据
     *
     * @param bannerList
     */
    private void initBannerData(List<Banner> bannerList) {
        Banner banner = null;
        for (int i = 0; i < bannerList.size(); i++) {
            banner = bannerList.get(i);
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .description(banner.getDescription())
                    .image(banner.getImgUrl()).setScaleType(BaseSliderView.ScaleType.Fit);
            textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {
                    Toast.makeText(getActivity(), slider.getDescription(), Toast.LENGTH_SHORT).show();
                }
            });
            BKSlider.addSlider(textSliderView);
        }
        BKSlider.setCustomIndicator(BKCustomIndicator);//定义指示器
        BKSlider.setCustomAnimation(new DescriptionAnimation());//定义下面的说明view的动画
        BKSlider.setPagerTransformer(false, new FlipHorizontalTransformer());////定义页面转场动画
        BKSlider.setDuration(2000);//设置停留时间
        BKSlider.addOnPageChangeListener(pageChangeListener);//添加页面切换监听器
        BKSlider.startAutoCycle();//开始循环
    }

    /**
     * 初始化RecycleView
     */
    private void initRecycleView() {
        layoutManager = new LinearLayoutManager(getActivity());
        BKRecycleView.setLayoutManager(layoutManager);
        List<Campaign> datas = new ArrayList<>();

        adapter = new HomeCategoryAdapter(getActivity(), datas);
        adapter.setOnCampaignItemClickListener(onCampaignItemClickListener);
        BKRecycleView.setAdapter(adapter);

//        BKRecycleView.addItemDecoration(new ItemDecoration());


    }

    @Override
    public void onStop() {
        BKSlider.stopAutoCycle();
        super.onStop();
    }


}
