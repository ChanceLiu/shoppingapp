package com.itheima.shoppingapp.activity.fragment;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.itheima.shoppingapp.R;
import com.itheima.shoppingapp.adapter.BaseAdapter;
import com.itheima.shoppingapp.adapter.ShopcartAdapter;
import com.itheima.shoppingapp.bean.ShoppingCart;
import com.itheima.shoppingapp.util.CartProvider;
import com.itheima.shoppingapp.widget.HeimaToolBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by Ivan on 15/9/22.
 */
public class CartFragment extends BaseFragment {


    @BindView(R.id.toolbar)
    HeimaToolBar BKToolbar;
    @BindView(R.id.recycler_view)
    RecyclerView BKRecyclerView;
    @BindView(R.id.checkbox_all)
    CheckBox BKCheckboxAll;
    @BindView(R.id.txt_total)
    TextView BKTxtTotal;
    @BindView(R.id.btn_order)
    Button BKBtnOrder;
    @BindView(R.id.btn_del)
    Button BKBtnDel;
    private List<ShoppingCart> shoppingCarts;
    private ShopcartAdapter mAdapter;

    private boolean isEditModel = false;
    private CartProvider cartProvider;

    @Override
    int getLayout() {
        return R.layout.fragment_cart;
    }

    @Override
    public void onResume() {
        super.onResume();
        BKBtnDel.setVisibility(isEditModel ? View.VISIBLE : View.GONE);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void initData() {
        EventBus.getDefault().register(this);
        BKToolbar.setOnRightButtonClickListener(new HeimaToolBar.OnRightButtonClickListener() {
            @Override
            public void onClick(View view) {
                setEditModel(!isEditModel);//设置编辑状态
            }
        });



        cartProvider = new CartProvider(getContext());
        shoppingCarts = cartProvider.getAll();
        BKRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        BKRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new ShopcartAdapter(shoppingCarts, getActivity());
        BKRecyclerView.setAdapter(mAdapter);
        mAdapter.setPriceTotalTv(BKTxtTotal);
        mAdapter.showTotalPrice();
        mAdapter.setOnItemClickListener(onItemClickListener);

    }
    BaseAdapter.OnItemClickListener onItemClickListener=new BaseAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            if (isEditModel) {
                shoppingCarts.get(position).isSelected = !shoppingCarts.get(position).isSelected;
                mAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(getContext(), "去商品详情功能待开发", Toast.LENGTH_SHORT).show();
            }
        }
    };



    @OnClick({R.id.btn_order, R.id.btn_del, R.id.checkbox_all})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_order:
                Toast.makeText(getContext(), "结算", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_del:
                for (ShoppingCart sc :
                        shoppingCarts) {
                    cartProvider.delete(sc);
                }
                mAdapter.refreshData(cartProvider.getAll());
                break;
            case R.id.checkbox_all:
                mAdapter.selectAll(BKCheckboxAll.isChecked());
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshData(ShopCartEvent shopCartEvent) {
        if (shopCartEvent.code == ShopCartEvent.OFF_PAGE) {
            setEditModel(false);
        } else if (shopCartEvent.code == ShopCartEvent.UPDATE_DATA) {
            mAdapter.refreshData(cartProvider.getAll());
        } else if (shopCartEvent.code == ShopCartEvent.SELECT_ALL_STATUE_CHANGE) {
            boolean isAllItemSelected = mAdapter.isAllItemSelected();
            BKCheckboxAll.setChecked(isAllItemSelected);
        }
    }

    public void setEditModel(boolean isEditModel) {
        this.isEditModel = isEditModel;
        BKBtnDel.setVisibility(isEditModel ? View.VISIBLE : View.GONE);
        BKToolbar.setRightButton(isEditModel ? getString(R.string.finish) : getString(R.string.edit));
        BKTxtTotal.setVisibility(isEditModel ? View.GONE : View.VISIBLE);
        mAdapter.selectAll(!isEditModel);//从产品的角度考虑：当编辑的时候，取消全选；完成编辑的时候进行全选
        BKCheckboxAll.setChecked(!isEditModel);//当编辑的时候全不选

    }


    public static class ShopCartEvent {
        /**
         * 离开页面
         */
        public static final int OFF_PAGE = 1;
        /**
         * 更新数据
         */
        public static final int UPDATE_DATA = 2;
        /**
         * 更新数据
         */
        public static final int SELECT_ALL_STATUE_CHANGE = 3;
        public int code;

        public ShopCartEvent(int code) {
            this.code = code;
        }

    }
}
