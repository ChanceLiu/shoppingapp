package com.itheima.shoppingapp.activity.fragment;

/**
     * 多数据加载状态
     */
    enum Statues {
        NORMAL, REFRESH, LOAD_MORE
    }
